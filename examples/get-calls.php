<?php


require_once dirname(__DIR__) . '/vendor/autoload.php';

use Dotenv\Dotenv;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\HttpFactory;
use Uplinestudio\CallTouchApi\CallTouchClient;
use Uplinestudio\CallTouchApi\CallTouchCredentials;
use Uplinestudio\CallTouchApi\Request\CallJournalRequest;


$dotenv = Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$creds = new CallTouchCredentials($_ENV['SITE_ID'], $_ENV['TOKEN']);


$httpClient = new Client();
$httpFactory = new HttpFactory();
$mangoClient = new CallTouchClient(
    $httpClient,
    $httpFactory,
    $httpFactory,
    $creds
);