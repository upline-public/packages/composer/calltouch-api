<?php

namespace Uplinestudio\CallTouchApi;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Uplinestudio\CallTouchApi\Request\CallJournalRequest;

class CallTouchClient
{
    const API_DOMAIN = 'https://api.calltouch.ru';

    private ClientInterface $client;
    private RequestFactoryInterface $requestFactory;
    private StreamFactoryInterface $streamFactory;
    private CallTouchCredentials $callTouchCredentials;

    public function __construct(
        ClientInterface         $client,
        RequestFactoryInterface $requestFactory,
        StreamFactoryInterface  $streamFactory,
        CallTouchCredentials    $callTouchCredentials
    )
    {
        $this->client = $client;
        $this->requestFactory = $requestFactory;
        $this->streamFactory = $streamFactory;
        $this->callTouchCredentials = $callTouchCredentials;
    }

    public function jsonGet(string $url): array
    {
        $request = $this->requestFactory->createRequest('GET', $url);

        $response = $this->client->sendRequest($request);
        if ($response->getStatusCode() !== 200) {
            throw new \RuntimeException('Wrong http answer', $response->getStatusCode());
        }
        return json_decode($response->getBody()->getContents(), 1);
    }

    public function getCalls(CallJournalRequest $callJournalRequest): array
    {
        $params = $callJournalRequest->toArray();
        $params['clientApiId'] = $this->callTouchCredentials->getToken();
        return $this->jsonGet(self::API_DOMAIN . '/calls-service/RestAPI/' . $this->callTouchCredentials->getSiteId() . '//calls-diary/calls?' . http_build_query($params));
    }

}