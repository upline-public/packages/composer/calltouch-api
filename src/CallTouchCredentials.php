<?php

namespace Uplinestudio\CallTouchApi;

class CallTouchCredentials
{
    private string $siteId;
    private string $token;

    public function __construct(
        string $siteId,
        string $token
    )
    {
        $this->siteId = $siteId;
        $this->token = $token;
    }

    public function getSiteId(): string
    {
        return $this->siteId;
    }

    public function getToken(): string
    {
        return $this->token;
    }
}