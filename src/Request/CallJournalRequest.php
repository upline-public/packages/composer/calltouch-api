<?php

namespace Uplinestudio\CallTouchApi\Request;

class CallJournalRequest
{
    private string $dateFrom;
    private string $dateTo;
    private ?bool $uniqueOnly = null;
    private ?bool $targetOnly = null;

    public function __construct(string $dateFrom, string $dateTo)
    {
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    public function toArray(): array
    {
        $params = [
            'dateFrom' => date('d/m/Y', strtotime($this->dateFrom)),
            'dateTo' => date('d/m/Y', strtotime($this->dateTo)),
        ];
        if ($this->uniqueOnly !== null) {
            $params['uniqueOnly'] = $this->uniqueOnly;
        }
        if ($this->targetOnly !== null) {
            $params['targetOnly'] = $this->targetOnly;
        }
        return $params;
    }

    /**
     * @param bool $uniqueOnly
     * @return CallJournalRequest
     */
    public function setUniqueOnly(bool $uniqueOnly): CallJournalRequest
    {
        $this->uniqueOnly = $uniqueOnly;
        return $this;
    }

    /**
     * @param bool $targetOnly
     * @return CallJournalRequest
     */
    public function setTargetOnly(bool $targetOnly): CallJournalRequest
    {
        $this->targetOnly = $targetOnly;
        return $this;
    }
}